import { Controller, Post, Body } from '@nestjs/common';
import { LoginDTO } from './dto/loginDTO';
import { AuthService } from './auth.service';
import { CustomersDTO } from '../customers/dto/customers.dto';
import {
  ApiBearerAuth,
  ApiOperation,
  ApiResponse,
  ApiTags,
} from '@nestjs/swagger';
import { RefreshTokenDTO } from './dto/refresh-token.dto';

@ApiTags('Auth')
@ApiBearerAuth()
@Controller('auth')
export class AuthController {
  constructor(private readonly authService: AuthService) {}

  @ApiOperation({ summary: 'Create a new user' })
  @ApiResponse({ status: 201, description: 'User created' })
  @ApiResponse({ status: 400, description: 'Bad Request' })
  @Post('login')
  async create(@Body() createCustomerDto: CustomersDTO) {
    return this.authService.create(createCustomerDto);
  }

  @ApiOperation({ summary: 'User login' })
  @ApiResponse({ status: 201, description: 'User logged in' })
  @ApiResponse({ status: 400, description: 'Bad Request' })
  @Post('signIn')
  async signIn(@Body() data: LoginDTO) {
    return this.authService.signIn(data);
  }

  @ApiOperation({ summary: 'Refresh user tokens' })
  @ApiResponse({ status: 201, description: 'Tokens refreshed' })
  @ApiResponse({ status: 400, description: 'Bad Request' })
  @Post('refreshToken')
  async refreshTokens(@Body() refreshData: RefreshTokenDTO) {
    return this.authService.refreshTokens(refreshData.id);
  }
}
