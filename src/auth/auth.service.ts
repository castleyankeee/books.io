import {
  Injectable,
  BadRequestException,
  NotFoundException,
  ForbiddenException,
} from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { ConfigService } from '@nestjs/config';
import { LoginDTO } from './dto/loginDTO';
import { InjectRepository } from '@nestjs/typeorm';
import { CustomersEntity } from '../customers/entities/customers.entity';
import { Repository } from 'typeorm';
import { CustomersDTO } from '../customers/dto/customers.dto';
import { RoleEnum } from '../customers/enums/role.enum';
// eslint-disable-next-line @typescript-eslint/no-var-requires
const bcrypt = require('bcrypt');

@Injectable()
export class AuthService {
  constructor(
    @InjectRepository(CustomersEntity)
    private readonly customersRepository: Repository<CustomersEntity>,
    private jwtService: JwtService,
    private configService: ConfigService,
  ) {}

  async create(createCustomerDto: CustomersDTO): Promise<CustomersEntity> {
    const existingCustomer = await this.customersRepository.findOne({
      where: { email: createCustomerDto.email },
    });

    if (existingCustomer) {
      throw new BadRequestException('User with such email already exists');
    }
    const hashPassword = await bcrypt.hash(createCustomerDto.password, 10);
    const newCustomer = this.customersRepository.create({
      ...createCustomerDto,
      password: hashPassword,
    });
    return this.customersRepository.save(newCustomer);
  }
  async signIn(data: LoginDTO) {
    const customer = await this.customersRepository.findOneBy({
      email: data.email,
    });
    if (!customer) throw new NotFoundException('User does not exist');
    const passwordMatches = bcrypt.compareSync(
      data.password,
      customer.password,
    );
    if (!passwordMatches)
      throw new BadRequestException('Password is incorrect');
    const tokens = await this.getTokens(
      customer.id,
      customer.email,
      customer.role,
    );
    await this.updateRefreshToken(customer.id, tokens.refreshToken);
    return tokens;
  }

  async refreshTokens(userId: string) {
    const customer = await this.customersRepository.findOneBy({ id: userId });
    if (!customer || !customer.refresh_token)
      throw new ForbiddenException('Access Denied');
    const tokens = await this.getTokens(
      customer.id,
      customer.email,
      customer.role,
    );
    await this.updateRefreshToken(customer.id, tokens.refreshToken);
    return tokens;
  }

  private async getTokens(id: string, email: string, role: RoleEnum) {
    const [accessToken, refreshToken] = await Promise.all([
      this.jwtService.signAsync(
        {
          id,
          email,
          role,
        },
        {
          secret: this.configService.get<string>('SECRET_KEY'),
          expiresIn: '15m',
        },
      ),
      this.jwtService.signAsync(
        {
          id,
          email,
          role,
        },
        {
          secret: this.configService.get<string>('SECRET_KEY'),
          expiresIn: '7d',
        },
      ),
    ]);

    return {
      accessToken,
      refreshToken,
    };
  }
  private async updateRefreshToken(id: string, refresh_token: string) {
    const hashedRefreshToken = await this.hashData(refresh_token);
    try {
      await this.customersRepository.update(id, {
        refresh_token: hashedRefreshToken,
      });
    } catch (error) {
      throw new Error('Failed to update refresh token');
    }
  }
  private hashData(data: string) {
    return bcrypt.hash(data, 10);
  }
  async checkRole(userId: string) {
    const user = await this.customersRepository.findOneBy({ id: userId });
    return user.role;
  }
}
