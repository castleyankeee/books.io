import { IsNotEmpty, IsOptional } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

export class RefreshTokenDTO {
  @IsNotEmpty()
  @IsOptional()
  id: string;

  @IsNotEmpty()
  @ApiProperty({ description: 'JWT refresh token' })
  refreshToken: string;
}
