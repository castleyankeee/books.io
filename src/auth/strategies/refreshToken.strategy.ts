import { PassportStrategy } from '@nestjs/passport';
import { ExtractJwt, Strategy } from 'passport-jwt';
import { Request } from 'express';
import { Injectable } from '@nestjs/common';
import * as process from 'process';
// eslint-disable-next-line @typescript-eslint/no-var-requires,@typescript-eslint/no-unused-vars
const dotenv = require('dotenv').config();

@Injectable()
export class RefreshTokenStrategy extends PassportStrategy(
  Strategy,
  'jwt_refresh',
) {
  constructor() {
    super({
      jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
      secretOrKey: process.env.SECRET_KEY,
      passReqToCallback: true,
    });
  }

  validate(req: Request, payload: any) {
    const refresh_token = req
      .get('Authorization')
      .replace('Bearer ', '')
      .trim();
    return { ...payload, refresh_token };
  }
}
