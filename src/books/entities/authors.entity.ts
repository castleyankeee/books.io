import { ApiProperty } from '@nestjs/swagger';
import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from 'typeorm';
import { BooksAuthorEntity } from './books-author.entity';

@Entity({ name: 'authors' })
export class AuthorsEntity {
  @PrimaryGeneratedColumn('uuid')
  @ApiProperty()
  id: string;

  @Column()
  @ApiProperty()
  name: string;

  @OneToMany(() => BooksAuthorEntity, (books_author) => books_author.authors)
  books_author: BooksAuthorEntity[];
}
