import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from 'typeorm';
import { ApiProperty } from '@nestjs/swagger';
import { BooksEntity } from './books.entity';

@Entity({ name: 'book_language' })
export class BookLanguageEntity {
  @PrimaryGeneratedColumn('uuid')
  @ApiProperty()
  id: string;

  @Column()
  @ApiProperty()
  language_name: string;

  @Column()
  @ApiProperty()
  language_code: string;

  @OneToMany(() => BooksEntity, (books) => books.book_language)
  books: BooksEntity[];
}
