import {
  Column,
  Entity,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { ApiProperty } from '@nestjs/swagger';
import { BooksEntity } from './books.entity';
import { AuthorsEntity } from './authors.entity';

@Entity({ name: 'books_author' })
export class BooksAuthorEntity {
  @PrimaryGeneratedColumn('uuid')
  @ApiProperty()
  id: string;

  @ManyToOne(() => AuthorsEntity, (authors) => authors.books_author)
  @JoinColumn({ name: 'author_id' })
  authors: AuthorsEntity;

  @Column()
  @ApiProperty()
  author_id: string;

  @ManyToOne(() => BooksEntity, (books) => books.books_author)
  @JoinColumn({ name: 'book_id' })
  books: BooksEntity;

  @Column()
  @ApiProperty()
  book_id: string;
}
