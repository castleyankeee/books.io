import {
  Column,
  Entity,
  JoinColumn,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { ApiProperty } from '@nestjs/swagger';
import { OrderLineEntity } from '../../orders/entities/order-line.entity';
import { BookLanguageEntity } from './book-language.entity';
import { BooksAuthorEntity } from './books-author.entity';

@Entity({ name: 'books' })
export class BooksEntity {
  @PrimaryGeneratedColumn('uuid')
  @ApiProperty()
  id: string;

  @Column()
  @ApiProperty()
  title: string;

  @Column()
  @ApiProperty()
  author: string;

  @Column()
  @ApiProperty()
  num_pages: number;

  @Column({ type: 'date' })
  @ApiProperty()
  publication_date: Date;

  @OneToMany(() => OrderLineEntity, (order_line) => order_line.books)
  order_line: OrderLineEntity[];

  @OneToMany(() => BooksAuthorEntity, (books_author) => books_author.books)
  books_author: BooksAuthorEntity[];

  @ManyToOne(() => BookLanguageEntity, (book_language) => book_language.books)
  @JoinColumn({ name: 'language_id' })
  book_language: BookLanguageEntity;

  @Column()
  @ApiProperty()
  language_id: string;
}
