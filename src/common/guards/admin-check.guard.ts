import { Injectable, CanActivate, ExecutionContext } from '@nestjs/common';
import { Reflector } from '@nestjs/core';
import { ROLES_KEY } from '../roles.decorator';
import { RoleEnum } from '../../customers/enums/role.enum';
import { JwtService } from '@nestjs/jwt';
import * as process from 'process';
import { AuthService } from '../../auth/auth.service';
@Injectable()
export class RolesGuard implements CanActivate {
  constructor(
    private authService: AuthService,
    private jwtService: JwtService,
    private reflector: Reflector,
  ) {}

  async canActivate(context: ExecutionContext): Promise<boolean> {
    const request = context.switchToHttp().getRequest();
    const authHeader = request.headers['authorization'];
    const token = authHeader.split('Bearer ')[1];

    try {
      const userDecoded = this.jwtService.verify(token, {
        secret: process.env.SECRET_KEY,
      });
      const userDatabase = await this.authService.checkRole(userDecoded.id);
      if (!userDatabase || !userDecoded.role) {
        return false;
      }
      const requiredRoles = this.reflector.getAllAndOverride<RoleEnum[]>(
        ROLES_KEY,
        [context.getHandler(), context.getClass()],
      );
      if (!requiredRoles) {
        return true;
      }

      const hasRequiredRole = requiredRoles.some(
        (requiredRole) => userDatabase?.includes(requiredRole),
      );

      return hasRequiredRole;
    } catch (error) {
      return false;
    }
  }
}
