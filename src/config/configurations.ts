import * as dotenv from 'dotenv';
import * as process from 'process';
dotenv.config();

const main = {
  port: process.env.PORT,
  secret_key: process.env.SECRET_KEY,
};
export { main };
