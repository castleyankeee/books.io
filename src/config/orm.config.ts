// eslint-disable-next-line @typescript-eslint/no-var-requires,@typescript-eslint/no-unused-vars
const dotenv = require('dotenv').config();
import { TypeOrmModuleOptions } from '@nestjs/typeorm';

export const typeormModuleOptions: TypeOrmModuleOptions = {
  type: 'postgres',
  url: process.env.DATABASE_URL,
  synchronize: false,
  migrationsRun: true,
  migrationsTableName: 'migrations_info',
  migrationsTransactionMode: 'all',
  entities: [__dirname + '/../**/*.entity.{ts,js}'],
  migrations: [__dirname + '/../migrations/*.{ts.js}'],
};
