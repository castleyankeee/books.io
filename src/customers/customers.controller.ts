import { Body, Controller, Get, Param, Patch, UseGuards } from '@nestjs/common';
import { CustomersDTO } from './dto/customers.dto';
import { CustomersService } from './customers.service';
import { AccessTokenGuard } from '../common/guards/access-token.guard';
import {
  ApiBearerAuth,
  ApiOperation,
  ApiResponse,
  ApiTags,
} from '@nestjs/swagger';
import { OwnershipCheckGuard } from '../common/guards/ownership-check.guard';
import { Roles } from '../common/roles.decorator';
import { RoleEnum } from './enums/role.enum';
import { RolesGuard } from '../common/guards/admin-check.guard';

@ApiTags('Customers')
@ApiBearerAuth()
@UseGuards(AccessTokenGuard)
@UseGuards(RolesGuard)
@Controller('customers')
export class CustomersController {
  constructor(private readonly customersService: CustomersService) {}

  @ApiOperation({ summary: 'Get all users' })
  @ApiResponse({ status: 201, description: 'Successfully' })
  @ApiResponse({ status: 400, description: 'Bad Request' })
  @Roles(RoleEnum.ADMIN)
  @Get('/')
  findAll() {
    return this.customersService.findAll();
  }

  @ApiOperation({ summary: 'Get user by id' })
  @ApiResponse({ status: 201, description: 'Successfully' })
  @ApiResponse({ status: 400, description: 'Bad Request' })
  @Roles(RoleEnum.ADMIN)
  @Get(':id')
  findById(@Param('id') id: string) {
    return this.customersService.findById(id);
  }

  @ApiOperation({ summary: 'Change user data' })
  @ApiResponse({ status: 201, description: 'User data changed successfully' })
  @ApiResponse({ status: 400, description: 'Bad Request' })
  @UseGuards(OwnershipCheckGuard)
  @Patch(':id')
  update(@Param('id') id: string, @Body() updateCustomersDto: CustomersDTO) {
    return this.customersService.update(id, updateCustomersDto);
  }
}
