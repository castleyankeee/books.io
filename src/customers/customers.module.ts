import { Module } from '@nestjs/common';
import { CustomersService } from './customers.service';
import { CustomersController } from './customers.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { CustomersEntity } from './entities/customers.entity';
import { CustomerAdressesEntity } from './entities/customer-adresses.entity';
import { CountriesEntity } from './entities/countries.entity';
import { AdressesEntity } from './entities/adresses.entity';
import { PassportModule } from '@nestjs/passport';
import { AuthService } from '../auth/auth.service';
import { JwtModule, JwtService } from '@nestjs/jwt';
import { AuthModule } from '../auth/auth.module';

@Module({
  imports: [
    AuthModule,
    PassportModule,
    JwtModule.register({}),
    TypeOrmModule.forFeature([
      CustomersEntity,
      CustomerAdressesEntity,
      CountriesEntity,
      AdressesEntity,
    ]),
  ],
  providers: [CustomersService, AuthService, JwtService],
  controllers: [CustomersController],
  exports: [CustomersService],
})
export class CustomersModule {}
