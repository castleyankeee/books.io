import { Injectable, BadRequestException } from '@nestjs/common';
import { CustomersDTO } from './dto/customers.dto';
import { CustomersEntity } from './entities/customers.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { ApiBearerAuth, ApiResponse, ApiTags } from '@nestjs/swagger';
import { AuthService } from '../auth/auth.service';

@ApiTags('Customers')
@ApiBearerAuth()
@Injectable()
export class CustomersService {
  constructor(
    @InjectRepository(CustomersEntity)
    private readonly customersRepository: Repository<CustomersEntity>,
    private authService: AuthService,
  ) {}

  async findAll(): Promise<CustomersEntity[]> {
    const getCustomers = await this.customersRepository.find();
    if (getCustomers.length === 0) {
      throw new BadRequestException('Customers not found');
    }
    return getCustomers;
  }

  async findById(id: string): Promise<CustomersEntity> {
    return this.customersRepository.findOneBy({ id });
  }

  async update(id: string, updateCustomerDto: CustomersDTO): Promise<string> {
    const existingCustomer = await this.customersRepository.findOneBy({
      id: id,
    });

    if (!existingCustomer) {
      throw new BadRequestException('Customer not found');
    }

    const updatedCustomer = { ...existingCustomer, ...updateCustomerDto };
    await this.customersRepository.save(updatedCustomer);
    try {
      const tokens = await this.authService.refreshTokens(id);

      return `AccessToken: ${tokens.accessToken}`;
    } catch (error) {
      throw new BadRequestException('Internal server error');
    }
  }
}
