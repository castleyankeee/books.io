import {
  IsEmail,
  IsEnum,
  IsLowercase,
  IsNotEmpty,
  IsString,
} from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';
import { RoleEnum } from '../enums/role.enum';

export class CustomersDTO {
  @IsNotEmpty()
  @IsString()
  @ApiProperty({ description: 'First name' })
  first_name: string;

  @IsNotEmpty()
  @IsString()
  @ApiProperty({ description: 'Last name' })
  last_name: string;

  @IsNotEmpty()
  @IsLowercase()
  @IsEmail()
  @ApiProperty({ description: 'User email' })
  email: string;

  @IsNotEmpty()
  @IsString()
  @ApiProperty({ description: 'User password' })
  password: string;

  @IsNotEmpty()
  @ApiProperty({ description: 'JWT refresh token' })
  refresh_token: null | string;

  @IsNotEmpty()
  @ApiProperty({ description: 'User Role' })
  @IsEnum(RoleEnum)
  role: RoleEnum;
}
