import {
  Column,
  Entity,
  JoinColumn,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { CountriesEntity } from './countries.entity';
import { CustomerAdressesEntity } from './customer-adresses.entity';
import { ApiProperty } from '@nestjs/swagger';

@Entity({ name: 'adresses' })
export class AdressesEntity {
  @PrimaryGeneratedColumn('uuid')
  @ApiProperty()
  id: string;

  @Column()
  @ApiProperty()
  street_name: string;

  @Column()
  @ApiProperty()
  street_number: number;

  @Column()
  @ApiProperty()
  city: string;

  @OneToMany(
    () => CustomerAdressesEntity,
    (customer_adresses) => customer_adresses.adress,
  )
  customer_adresses: CustomerAdressesEntity[];

  @ManyToOne(() => CountriesEntity, (country) => country.adresses)
  @JoinColumn({ name: 'country_id' })
  country: CountriesEntity;

  @Column()
  @ApiProperty()
  country_id: string;
}
