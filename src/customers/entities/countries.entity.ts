import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from 'typeorm';
import { AdressesEntity } from './adresses.entity';
import { ApiProperty } from '@nestjs/swagger';

@Entity({ name: 'countries' })
export class CountriesEntity {
  @PrimaryGeneratedColumn('uuid')
  @ApiProperty()
  id: string;

  @Column()
  @ApiProperty()
  country_name: string;

  @OneToMany(() => AdressesEntity, (adress) => adress.country)
  adresses: AdressesEntity[];
}
