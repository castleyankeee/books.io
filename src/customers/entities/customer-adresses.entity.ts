import {
  Column,
  Entity,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { StatusEnum } from '../enums/status.enum';
import { AdressesEntity } from './adresses.entity';
import { CustomersEntity } from './customers.entity';
import { ApiProperty } from '@nestjs/swagger';

@Entity({ name: 'customer_adresses' })
export class CustomerAdressesEntity {
  @PrimaryGeneratedColumn('uuid')
  @ApiProperty()
  id: string;

  @Column({
    type: 'enum',
    enum: StatusEnum,
  })
  @ApiProperty()
  status: StatusEnum;

  @ManyToOne(() => AdressesEntity, (adress) => adress.customer_adresses)
  @JoinColumn({ name: 'adress_id' })
  adress: AdressesEntity;

  @Column()
  @ApiProperty()
  adress_id: string;

  @ManyToOne(() => CustomersEntity, (customers) => customers.customer_adresses)
  @JoinColumn({ name: 'customer_id' })
  customers: CustomersEntity;

  @Column()
  @ApiProperty()
  customer_id: string;
}
