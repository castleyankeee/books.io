import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from 'typeorm';
import { CustomerAdressesEntity } from './customer-adresses.entity';
import { ApiProperty } from '@nestjs/swagger';
import { CustomerOrdersEntity } from '../../orders/entities/customer-orders.entity';
import { RoleEnum } from '../enums/role.enum';

@Entity({ name: 'customers' })
export class CustomersEntity {
  @PrimaryGeneratedColumn('uuid')
  @ApiProperty()
  id: string;

  @Column()
  @ApiProperty()
  first_name: string;

  @Column()
  @ApiProperty()
  last_name: string;

  @Column({ unique: true })
  @ApiProperty()
  email: string;

  @Column()
  @ApiProperty()
  password: string;

  @ApiProperty()
  @Column({
    type: 'enum',
    enum: RoleEnum,
    default: RoleEnum.USER,
  })
  role: RoleEnum;

  @ApiProperty()
  @Column({
    nullable: true,
  })
  refresh_token: string;
  @OneToMany(
    () => CustomerAdressesEntity,
    (customer_adresses) => customer_adresses.customers,
  )
  customer_adresses: CustomerAdressesEntity[];

  @OneToMany(
    () => CustomerOrdersEntity,
    (customer_orders) => customer_orders.customers,
  )
  customer_orders: CustomerOrdersEntity[];
}
