import { MigrationInterface, QueryRunner } from "typeorm";

export class InitTables1697182008636 implements MigrationInterface {
    name = 'InitTables1697182008636'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`CREATE TABLE "order_status" ("id" uuid NOT NULL DEFAULT uuid_generate_v4(), "status_value" character varying NOT NULL, CONSTRAINT "PK_8ea75b2a26f83f3bc98b9c6aaf6" PRIMARY KEY ("id"))`);
        await queryRunner.query(`CREATE TABLE "orders_history" ("id" uuid NOT NULL DEFAULT uuid_generate_v4(), "status_date" date NOT NULL, "status_id" uuid NOT NULL, "order_id" uuid NOT NULL, CONSTRAINT "PK_0053f303fce5fc77fed8f1095d3" PRIMARY KEY ("id"))`);
        await queryRunner.query(`CREATE TABLE "countries" ("id" uuid NOT NULL DEFAULT uuid_generate_v4(), "country_name" character varying NOT NULL, CONSTRAINT "PK_b2d7006793e8697ab3ae2deff18" PRIMARY KEY ("id"))`);
        await queryRunner.query(`CREATE TABLE "adresses" ("id" uuid NOT NULL DEFAULT uuid_generate_v4(), "street_name" character varying NOT NULL, "street_number" integer NOT NULL, "city" character varying NOT NULL, "country_id" uuid NOT NULL, CONSTRAINT "PK_2787c84f7433e390ff8961d552d" PRIMARY KEY ("id"))`);
        await queryRunner.query(`CREATE TABLE "customer_adresses" ("id" uuid NOT NULL DEFAULT uuid_generate_v4(), "status" "public"."customer_adresses_status_enum" NOT NULL, "adress_id" uuid NOT NULL, "customer_id" uuid NOT NULL, CONSTRAINT "PK_29b15574d0a7f66e29ecc362e04" PRIMARY KEY ("id"))`);
        await queryRunner.query(`CREATE TABLE "customers" ("id" uuid NOT NULL DEFAULT uuid_generate_v4(), "first_name" character varying NOT NULL, "last_name" character varying NOT NULL, "email" character varying NOT NULL, "password" character varying NOT NULL, "role" "public"."customers_role_enum" NOT NULL DEFAULT 'user', "refresh_token" character varying, CONSTRAINT "UQ_8536b8b85c06969f84f0c098b03" UNIQUE ("email"), CONSTRAINT "PK_133ec679a801fab5e070f73d3ea" PRIMARY KEY ("id"))`);
        await queryRunner.query(`CREATE TABLE "shipping_methods" ("id" uuid NOT NULL DEFAULT uuid_generate_v4(), "method_name" character varying NOT NULL, "cost" integer NOT NULL, CONSTRAINT "PK_5bee9dd62a8b72d6d9caabd63cf" PRIMARY KEY ("id"))`);
        await queryRunner.query(`CREATE TABLE "customer_orders" ("id" uuid NOT NULL DEFAULT uuid_generate_v4(), "order_date" date NOT NULL, "customer_id" uuid NOT NULL, "shipping_methods_id" uuid NOT NULL, CONSTRAINT "PK_ce425b6edb31cce9a80b269298e" PRIMARY KEY ("id"))`);
        await queryRunner.query(`CREATE TABLE "order_line" ("id" uuid NOT NULL DEFAULT uuid_generate_v4(), "price" integer NOT NULL, "order_id" uuid NOT NULL, "books_id" uuid NOT NULL, CONSTRAINT "PK_01a7c973d9f30479647e44f9892" PRIMARY KEY ("id"))`);
        await queryRunner.query(`CREATE TABLE "book_language" ("id" uuid NOT NULL DEFAULT uuid_generate_v4(), "language_name" character varying NOT NULL, "language_code" character varying NOT NULL, CONSTRAINT "PK_cacabdc34524ac540a43b4413be" PRIMARY KEY ("id"))`);
        await queryRunner.query(`CREATE TABLE "books" ("id" uuid NOT NULL DEFAULT uuid_generate_v4(), "title" character varying NOT NULL, "author" character varying NOT NULL, "num_pages" integer NOT NULL, "publication_date" date NOT NULL, "language_id" uuid NOT NULL, CONSTRAINT "PK_f3f2f25a099d24e12545b70b022" PRIMARY KEY ("id"))`);
        await queryRunner.query(`CREATE TABLE "authors" ("id" uuid NOT NULL DEFAULT uuid_generate_v4(), "name" character varying NOT NULL, CONSTRAINT "PK_d2ed02fabd9b52847ccb85e6b88" PRIMARY KEY ("id"))`);
        await queryRunner.query(`CREATE TABLE "books_author" ("id" uuid NOT NULL DEFAULT uuid_generate_v4(), "author_id" uuid NOT NULL, "book_id" uuid NOT NULL, CONSTRAINT "PK_1056dbee4616479f7d562c562df" PRIMARY KEY ("id"))`);
        await queryRunner.query(`ALTER TABLE "orders_history" ADD CONSTRAINT "FK_963166319199cfeab64800d5318" FOREIGN KEY ("status_id") REFERENCES "order_status"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "orders_history" ADD CONSTRAINT "FK_e69f337a6123047c80ed1df6d41" FOREIGN KEY ("order_id") REFERENCES "customer_orders"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "adresses" ADD CONSTRAINT "FK_f6cb5e7fe5e58354e133747edf0" FOREIGN KEY ("country_id") REFERENCES "countries"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "customer_adresses" ADD CONSTRAINT "FK_3bdeb55836302e06b37594db02f" FOREIGN KEY ("adress_id") REFERENCES "adresses"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "customer_adresses" ADD CONSTRAINT "FK_f89efc6e86f74523a588e3a9a1e" FOREIGN KEY ("customer_id") REFERENCES "customers"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "customer_orders" ADD CONSTRAINT "FK_d7fd44c68cff957a9168272c745" FOREIGN KEY ("customer_id") REFERENCES "customers"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "customer_orders" ADD CONSTRAINT "FK_2ad86d896a4910534fc3356e1a8" FOREIGN KEY ("shipping_methods_id") REFERENCES "shipping_methods"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "order_line" ADD CONSTRAINT "FK_ed8fae6d7239e9d730219215af7" FOREIGN KEY ("order_id") REFERENCES "customer_orders"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "order_line" ADD CONSTRAINT "FK_b1a781b4401ef366ad185c5015c" FOREIGN KEY ("books_id") REFERENCES "books"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "books" ADD CONSTRAINT "FK_3164a2958d73d8cdebe5204c838" FOREIGN KEY ("language_id") REFERENCES "book_language"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "books_author" ADD CONSTRAINT "FK_b891f42b1a1959b09ad6dd55064" FOREIGN KEY ("author_id") REFERENCES "authors"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "books_author" ADD CONSTRAINT "FK_3aa607b8642e91662d0f663938c" FOREIGN KEY ("book_id") REFERENCES "books"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "books_author" DROP CONSTRAINT "FK_3aa607b8642e91662d0f663938c"`);
        await queryRunner.query(`ALTER TABLE "books_author" DROP CONSTRAINT "FK_b891f42b1a1959b09ad6dd55064"`);
        await queryRunner.query(`ALTER TABLE "books" DROP CONSTRAINT "FK_3164a2958d73d8cdebe5204c838"`);
        await queryRunner.query(`ALTER TABLE "order_line" DROP CONSTRAINT "FK_b1a781b4401ef366ad185c5015c"`);
        await queryRunner.query(`ALTER TABLE "order_line" DROP CONSTRAINT "FK_ed8fae6d7239e9d730219215af7"`);
        await queryRunner.query(`ALTER TABLE "customer_orders" DROP CONSTRAINT "FK_2ad86d896a4910534fc3356e1a8"`);
        await queryRunner.query(`ALTER TABLE "customer_orders" DROP CONSTRAINT "FK_d7fd44c68cff957a9168272c745"`);
        await queryRunner.query(`ALTER TABLE "customer_adresses" DROP CONSTRAINT "FK_f89efc6e86f74523a588e3a9a1e"`);
        await queryRunner.query(`ALTER TABLE "customer_adresses" DROP CONSTRAINT "FK_3bdeb55836302e06b37594db02f"`);
        await queryRunner.query(`ALTER TABLE "adresses" DROP CONSTRAINT "FK_f6cb5e7fe5e58354e133747edf0"`);
        await queryRunner.query(`ALTER TABLE "orders_history" DROP CONSTRAINT "FK_e69f337a6123047c80ed1df6d41"`);
        await queryRunner.query(`ALTER TABLE "orders_history" DROP CONSTRAINT "FK_963166319199cfeab64800d5318"`);
        await queryRunner.query(`DROP TABLE "books_author"`);
        await queryRunner.query(`DROP TABLE "authors"`);
        await queryRunner.query(`DROP TABLE "books"`);
        await queryRunner.query(`DROP TABLE "book_language"`);
        await queryRunner.query(`DROP TABLE "order_line"`);
        await queryRunner.query(`DROP TABLE "customer_orders"`);
        await queryRunner.query(`DROP TABLE "shipping_methods"`);
        await queryRunner.query(`DROP TABLE "customers"`);
        await queryRunner.query(`DROP TABLE "customer_adresses"`);
        await queryRunner.query(`DROP TABLE "adresses"`);
        await queryRunner.query(`DROP TABLE "countries"`);
        await queryRunner.query(`DROP TABLE "orders_history"`);
        await queryRunner.query(`DROP TABLE "order_status"`);
    }

}
