import 'reflect-metadata';
import { DataSource } from 'typeorm';
const dotenv = require('dotenv').config();

export const AppDataSource = new DataSource({
  type: 'postgres',
  host: 'localhost',
  port: 5432,
  username: 'postgres',
  password: 'admin',
  database: 'postgres',
  schema: 'public',
  logging: true,
  entities: [__dirname + '/../**/entities/*.entity.{ts,js}'],
  synchronize: false,
  migrations: ['__dirname + /../**/migrations/*.{ts,js}'],
  migrationsTableName: 'migrations_info',
  migrationsRun: true,
});
