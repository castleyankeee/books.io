import {
  Column,
  Entity,
  JoinColumn,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { ApiProperty } from '@nestjs/swagger';
import { OrdersHistoryEntity } from './orders-history.entity';
import { CustomersEntity } from '../../customers/entities/customers.entity';
import { ShippingMethodsEntity } from './shipping-methods.entity';
import { OrderLineEntity } from './order-line.entity';

@Entity({ name: 'customer_orders' })
export class CustomerOrdersEntity {
  @PrimaryGeneratedColumn('uuid')
  @ApiProperty()
  id: string;

  @Column({ type: 'date' })
  @ApiProperty()
  order_date: Date;

  @OneToMany(
    () => OrdersHistoryEntity,
    (orders_history) => orders_history.customer_orders,
  )
  order_history: OrdersHistoryEntity[];

  @OneToMany(() => OrderLineEntity, (order_line) => order_line.customer_orders)
  order_line: OrderLineEntity[];

  @ManyToOne(() => CustomersEntity, (customers) => customers.customer_orders)
  @JoinColumn({ name: 'customer_id' })
  customers: CustomersEntity;

  @Column()
  @ApiProperty()
  customer_id: string;

  @ManyToOne(
    () => ShippingMethodsEntity,
    (shipping_methods) => shipping_methods.customer_orders,
  )
  @JoinColumn({ name: 'shipping_methods_id' })
  shipping_methods: ShippingMethodsEntity;

  @Column()
  @ApiProperty()
  shipping_methods_id: string;
}
