import {
  Column,
  Entity,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { ApiProperty } from '@nestjs/swagger';
import { CustomerOrdersEntity } from './customer-orders.entity';
import { BooksEntity } from '../../books/entities/books.entity';

@Entity({ name: 'order_line' })
export class OrderLineEntity {
  @PrimaryGeneratedColumn('uuid')
  @ApiProperty()
  id: string;

  @Column()
  @ApiProperty()
  price: number;

  @ManyToOne(
    () => CustomerOrdersEntity,
    (customer_orders) => customer_orders.order_line,
  )
  @JoinColumn({ name: 'order_id' })
  customer_orders: CustomerOrdersEntity;

  @Column()
  @ApiProperty()
  order_id: string;

  @ManyToOne(() => BooksEntity, (books) => books.order_line)
  @JoinColumn({ name: 'books_id' })
  books: BooksEntity;

  @Column()
  @ApiProperty()
  books_id: string;
}
