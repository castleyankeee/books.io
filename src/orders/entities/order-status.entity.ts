import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from 'typeorm';
import { ApiProperty } from '@nestjs/swagger';
import { OrdersHistoryEntity } from './orders-history.entity';

@Entity({ name: 'order_status' })
export class OrderStatusEntity {
  @PrimaryGeneratedColumn('uuid')
  @ApiProperty()
  id: string;

  @Column()
  @ApiProperty()
  status_value: string;

  @OneToMany(
    () => OrdersHistoryEntity,
    (orders_history) => orders_history.order_status,
  )
  order_history: OrdersHistoryEntity[];
}
