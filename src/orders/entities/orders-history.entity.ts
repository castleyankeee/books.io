import {
  Column,
  Entity,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { ApiProperty } from '@nestjs/swagger';
import { OrderStatusEntity } from './order-status.entity';
import { CustomerOrdersEntity } from './customer-orders.entity';

@Entity({ name: 'orders_history' })
export class OrdersHistoryEntity {
  @PrimaryGeneratedColumn('uuid')
  @ApiProperty()
  id: string;

  @Column({ type: 'date' })
  @ApiProperty()
  status_date: Date;

  @ManyToOne(
    () => OrderStatusEntity,
    (order_status) => order_status.order_history,
  )
  @JoinColumn({ name: 'status_id' })
  order_status: OrderStatusEntity;

  @Column()
  @ApiProperty()
  status_id: string;

  @ManyToOne(
    () => CustomerOrdersEntity,
    (customer_orders) => customer_orders.order_history,
  )
  @JoinColumn({ name: 'order_id' })
  customer_orders: CustomerOrdersEntity;

  @Column()
  @ApiProperty()
  order_id: string;
}
