import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from 'typeorm';
import { ApiProperty } from '@nestjs/swagger';
import { CustomerOrdersEntity } from './customer-orders.entity';

@Entity({ name: 'shipping_methods' })
export class ShippingMethodsEntity {
  @PrimaryGeneratedColumn('uuid')
  @ApiProperty()
  id: string;

  @Column()
  @ApiProperty()
  method_name: string;

  @Column()
  @ApiProperty()
  cost: number;

  @OneToMany(
    () => CustomerOrdersEntity,
    (customer_orders) => customer_orders.shipping_methods,
  )
  customer_orders: CustomerOrdersEntity[];
}
